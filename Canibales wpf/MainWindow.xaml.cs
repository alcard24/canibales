﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Canibales_wpf
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {


        enum EstadoDelBote {
               Ida,
               Regreso,
               Viajando
        }

        enum Jugada {
            Correcta,
            Incorrecta
        }

        enum Tipos
        {
            Canibal,
            Vegetariano,
            Ninguno
        }
        int numeroDeCanibalesDesde;
        int numeroDeVegetarianosDesde;
        int numeroDeCanibalesHasta;
        int numeroDeVegetarianosHasta;

        List<Button> vegetarianosDesde;
        List<Button> canibalesDesde;

        List<Button> canibalesHasta;
        List<Button> vegetarianosHasta;
       
        
        private Tipos pas1;
        private Tipos pas2;
        private EstadoDelBote estadoDelBote;
        Tipos Pas1 {
            get {
                return pas1;
            }
            set {
                pas1 = value;

                    switch (pas1)
                {
                    case Tipos.Canibal:
                        psj1.Source = new BitmapImage((new Uri(@"\canibal_.png", UriKind.Relative)));
                        break;
                    case Tipos.Vegetariano:
                        psj1.Source = new BitmapImage((new Uri(@"cura.png", UriKind.Relative)));
                        break;
                    case Tipos.Ninguno:
                        psj1.Source = null;
                        break;
                    default:
                        break;
                }
            }
        }

       

        Tipos Pas2
        {
            get
            {
                return pas2;
            }
            set
            {
                pas2 = value;

                switch (pas2)
                {
                    case Tipos.Canibal:
                        psj2.Source = new BitmapImage((new Uri(@"\canibal_.png", UriKind.Relative)));
                        break;
                    case Tipos.Vegetariano:
                        psj2.Source = new BitmapImage((new Uri(@"cura.png", UriKind.Relative)));
                        break;
                    case Tipos.Ninguno:
                        psj2.Source = null;
                        break;
                    default:
                        break;
                }
            }
        }


        Jugada valorarJugada() {
            return (
                ( 
                ((numeroDeCanibalesDesde <= numeroDeVegetarianosDesde) || numeroDeVegetarianosDesde == 0)
                    && ((numeroDeCanibalesHasta <= numeroDeVegetarianosHasta) || numeroDeVegetarianosHasta == 0)
                ) ? Jugada.Correcta : Jugada.Incorrecta);
        }
        public MainWindow()
        {
            InitializeComponent();

            Pas1 = Tipos.Ninguno;
            Pas2 = Tipos.Ninguno;
            canibalesDesde = new List<Button>();
            vegetarianosDesde = new List<Button>();


            canibalesHasta = new List<Button>();
            vegetarianosHasta = new List<Button>();

            numeroDeCanibalesDesde = numeroDeVegetarianosDesde = 3;
            numeroDeCanibalesHasta = numeroDeVegetarianosHasta = 0;

            canibalesDesde.Add(canibalDesde1);
            canibalesDesde.Add(canibalDesde2);
            canibalesDesde.Add(canibalDesde3);

            vegetarianosDesde.Add(vegetarianoDesde1);
            vegetarianosDesde.Add(vegetarianoDesde2);
            vegetarianosDesde.Add(vegetarianoDesde3);

            canibalesHasta.Add(canibaleHasta1);
            canibalesHasta.Add(canibalHasta2);
            canibalesHasta.Add(canibalHasta3);

            vegetarianosHasta.Add(vegetarianoHasta1);
            vegetarianosHasta.Add(vegetarianoHasta2);
            vegetarianosHasta.Add(vegetarianoHasta3);

            foreach (var item in canibalesHasta)
            {
                item.Visibility = Visibility.Hidden;
                item.Click += canibal_hasta;
            }
            foreach (var item in vegetarianosHasta)
            {
                item.Visibility = Visibility.Hidden;
                item.Click += vegetariano_hasta;
            }
            foreach (var item in vegetarianosDesde)
            {

                item.Click += vegetariano_desde;
            }


            bajarPasajero2.Click += bajarPasajero2_Click;
            estadoDelBote = EstadoDelBote.Ida;
            this.WindowStyle = WindowStyle.ToolWindow;
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
        }



        private void canibal_desde(object sender, RoutedEventArgs e)
        {
            if (estadoDelBote == EstadoDelBote.Viajando)
            {
                // Se controlo el evento
                e.Handled = true;
                return;
            }
            if (Pas1 == Tipos.Ninguno)
            {
                Pas1 = Tipos.Canibal;
            }
            else{
                Pas2 = Tipos.Canibal;
            }
            numeroDeCanibalesDesde -= 1;
            (sender as Button).Visibility = Visibility.Hidden;

        }

        private void vegetariano_desde(object sender, RoutedEventArgs e)
        {
            if (estadoDelBote == EstadoDelBote.Viajando)
            {
                // Se controlo el evento
                e.Handled = true;
                return;
            }
            if (Pas1 == Tipos.Ninguno)
            {
                Pas1 = Tipos.Vegetariano;
            }
            else
            {
                Pas2 = Tipos.Vegetariano;
            }
            numeroDeVegetarianosDesde -= 1;
            (sender as Button).Visibility = Visibility.Hidden;

        }

        private void canibal_hasta(object sender, RoutedEventArgs e)
        {
            if (estadoDelBote == EstadoDelBote.Viajando)
            {
                // Se controlo el evento
                e.Handled = true;
                return;
            }
            if (Pas1 == Tipos.Ninguno)
            {
                Pas1 = Tipos.Canibal;
            }
            else
            {
                Pas2 = Tipos.Canibal;
            }
            numeroDeCanibalesHasta-= 1;
            (sender as Button).Visibility = Visibility.Hidden;

        }

        private void vegetariano_hasta(object sender, RoutedEventArgs e)
        {
            if (estadoDelBote == EstadoDelBote.Viajando)
            {
                // Se controlo el evento
                e.Handled = true;
                return;
            }
            if (Pas1 == Tipos.Ninguno)
            {
                Pas1 = Tipos.Vegetariano;
            }
            else
            {
                Pas2 = Tipos.Vegetariano;
            }
            numeroDeVegetarianosHasta -= 1;
            (sender as Button).Visibility = Visibility.Hidden;

        }


        private void enviar_Click(object sender, RoutedEventArgs e)
        {
            if (estadoDelBote == EstadoDelBote.Ida)
            {

                if (Pas1 == Tipos.Ninguno && Pas2 == Tipos.Ninguno)
                {
                    MessageBox.Show("El bote no se mueve solo !");
                    e.Handled = true;
                    return;
                }
                var timer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(0.2) };
                timer.Start();
                estadoDelBote = EstadoDelBote.Viajando;
                int updates = 0;
                timer.Tick += (send, args) =>
                {
                    updates += 1;
                    barco.Margin = new Thickness(barco.Margin.Left - 20 ,barco.Margin.Top,barco.Margin.Right,barco.Margin.Bottom);
                    if (updates == 6)
                    {
                        timer.Stop();
                        estadoDelBote = EstadoDelBote.Regreso;

                        switch (valorarJugada())
                        {
                            case Jugada.Correcta:
                                MessageBox.Show("Movimiento valido");
                                break;
                            case Jugada.Incorrecta:
                                MessageBox.Show("Perdiste :(, bye bye");
                                System.Windows.Application.Current.Shutdown();
                                break;
                            default:
                                break;
                        }
                    }
                };
            }
            else {

                var timer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(0.2) };
                timer.Start();
                int updates = 0;
                timer.Tick += (send, args) =>
                {
                    updates += 1;
                    barco.Margin = new Thickness(barco.Margin. Left + 20, barco.Margin.Top, barco.Margin.Right, barco.Margin.Bottom);
                    if (updates == 6)
                    {
                        timer.Stop();
                        estadoDelBote = EstadoDelBote.Ida;


                        switch (valorarJugada())
                        {
                            case Jugada.Correcta:
                                MessageBox.Show("Movimiento valido");
                                break;
                            case Jugada.Incorrecta:
                                MessageBox.Show("Perdiste :(, bye bye");
                                System.Windows.Application.Current.Shutdown();
                                break;
                            default:
                                break;
                        }
                    }
                };
            }
        }

        private void creditos_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Programado en c# con wpf \n Dessarrollado por Kevin Gaitan \n Para la clase de Sistemas Operativos, docente Engels Mijail", "Vegetarianos y Canibales");
        }

        private void bajarPasajero1_Click(object sender, RoutedEventArgs e)
        {
            Tipos actual = Pas1;
            if (Pas1 != Tipos.Ninguno) {
                if (estadoDelBote == EstadoDelBote.Ida)  {
                    
                    if (Pas1 == Tipos.Canibal) {
                        foreach (var item in canibalesDesde)
                        {
                            if (item.Visibility == Visibility.Hidden) {
                                item.Visibility = Visibility.Visible;
                                numeroDeCanibalesDesde += 1;
                                break; // Exit;
                            }
                        }
                    }

                    if (Pas1 == Tipos.Vegetariano)
                    {
                        foreach (var item in vegetarianosDesde)
                        {
                            if (item.Visibility == Visibility.Hidden)
                            {
                                item.Visibility = Visibility.Visible;
                                numeroDeVegetarianosDesde += 1;
                                break; // Exit;
                            }
                        }
                    }
                    Pas1 = Tipos.Ninguno;
                }

                if (estadoDelBote == EstadoDelBote.Regreso)
                {

                    if (Pas1 == Tipos.Canibal)
                    {
                        foreach (var item in canibalesHasta)
                        {
                            if (item.Visibility == Visibility.Hidden)
                            {
                                item.Visibility = Visibility.Visible;
                                numeroDeCanibalesHasta += 1;
                                break; // Exit;
                            }
                        }
                    }

                    if (Pas1 == Tipos.Vegetariano)
                    {
                        foreach (var item in vegetarianosHasta)
                        {
                            if (item.Visibility == Visibility.Hidden)
                            {
                                item.Visibility = Visibility.Visible;
                                numeroDeVegetarianosHasta += 1;
                                break; // Exit;
                            }
                        }
                    }
                    Pas1 = Tipos.Ninguno;
                }
            }
        }

        private void bajarPasajero2_Click(object sender, RoutedEventArgs e)
        {
            Tipos actual = Pas1;
            if (Pas2 != Tipos.Ninguno)
            {
                if (estadoDelBote == EstadoDelBote.Ida)
                {

                    if (Pas2 == Tipos.Canibal)
                    {
                        foreach (var item in canibalesDesde)
                        {
                            if (item.Visibility == Visibility.Hidden)
                            {
                                item.Visibility = Visibility.Visible;
                                numeroDeCanibalesDesde += 1;
                                break; // Exit;
                            }
                        }
                    }

                    if (Pas2 == Tipos.Vegetariano)
                    {
                        foreach (var item in vegetarianosDesde)
                        {
                            if (item.Visibility == Visibility.Hidden)
                            {
                                item.Visibility = Visibility.Visible;
                                numeroDeVegetarianosDesde += 1;
                                break; // Exit;
                            }
                        }
                    }
                    Pas2 = Tipos.Ninguno;
                }

                if (estadoDelBote == EstadoDelBote.Regreso)
                {

                    if (Pas2 == Tipos.Canibal)
                    {
                        foreach (var item in canibalesHasta)
                        {
                            if (item.Visibility == Visibility.Hidden)
                            {
                                item.Visibility = Visibility.Visible;
                                numeroDeCanibalesHasta += 1;
                                break; // Exit;
                            }
                        }
                    }

                    if (Pas2 == Tipos.Vegetariano)
                    {
                        foreach (var item in vegetarianosHasta)
                        {
                            if (item.Visibility == Visibility.Hidden)
                            {
                                item.Visibility = Visibility.Visible;
                                numeroDeVegetarianosHasta += 1;
                                break; // Exit;
                            }
                        }
                    }
                    Pas2 = Tipos.Ninguno;
                }
            }
        }



    }
}
